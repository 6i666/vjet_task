<?php

define('DS', DIRECTORY_SEPARATOR);
define('BASE_PATH', dirname(__DIR__) . DS);
define('CORE_PATH', BASE_PATH . 'core' . DS);
define('CONTROLLERS_PATH', BASE_PATH . 'controllers' . DS);
define('MODELS_PATH', BASE_PATH . 'models' . DS);
define('VIEWS_PATH', BASE_PATH . 'views' . DS);

// Load classes and set config.
require_once(BASE_PATH . '/vendor/autoload.php' );
require_once(CORE_PATH . 'init.php');

try {
    Core\App::run($_SERVER['REQUEST_URI']);
} catch (\Exception $e) {
    echo 'An error occurred: ' . $e->getMessage();
}
