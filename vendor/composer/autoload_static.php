<?php

// autoload_static.php @generated by Composer

namespace Composer\Autoload;

class ComposerStaticInitb5d7b9ce08ff6e82541a04d27fbf5fb1
{
    public static $prefixLengthsPsr4 = array (
        'a' => 
        array (
            'app\\' => 4,
        ),
        'C' => 
        array (
            'Core\\' => 5,
        ),
    );

    public static $prefixDirsPsr4 = array (
        'app\\' => 
        array (
            0 => __DIR__ . '/../..' . '/',
        ),
        'Core\\' => 
        array (
            0 => __DIR__ . '/../..' . '/core',
        ),
    );

    public static function getInitializer(ClassLoader $loader)
    {
        return \Closure::bind(function () use ($loader) {
            $loader->prefixLengthsPsr4 = ComposerStaticInitb5d7b9ce08ff6e82541a04d27fbf5fb1::$prefixLengthsPsr4;
            $loader->prefixDirsPsr4 = ComposerStaticInitb5d7b9ce08ff6e82541a04d27fbf5fb1::$prefixDirsPsr4;

        }, null, ClassLoader::class);
    }
}
