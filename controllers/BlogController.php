<?php

namespace app\controllers;

use app\models\Blog;
use app\models\Comments;

/**
 * Blog controller.
 */
class BlogController extends \Core\Controller
{
    /**
     * @inheritdoc
     */
    protected function init()
    {
        $this->model = new Blog();
    }

    /**
     * Blog main page action.
     */
    public function actionIndex()
    {
        // Get all posts.
        $this->data['posts'] = $this->model->getPosts();
    }

    /**
     * Full post action.
     */
    public function actionPost()
    {
        if (!empty($this->params)) {
            $id = array_shift($this->params);
            if (!is_numeric($id)) {
                header('Location: /');
            }
        }

        $post = $this->model->getPost($id);
        if (empty($post)) {
            header('Location: /');
        }

        // Load comments model.
        $comments = new Comments();

        // Create post if form was submited.
        if (!empty($_POST)) {
            $data = $_POST;
            $data['postId'] = $id;

            // Validation.
            if (empty($data['author'])) {
                $errors['author'] = 'This field is required!';
            }
            if (empty($data['comment'])) {
                $errors['comment'] = 'This field is required!';
            }

            if (empty($errors)) {
                if ($comments->createComment($data)) {
                    $result = [
                        'success' => true,
                        'message' => 'Comment created successfully!',
                    ];
                } else {
                    $result = [
                        'success' => true,
                        'message' => 'Error on comment creating!',
                    ];
                }
                $this->data['result'] = $result;
            } else {
                $this->data['errors'] = $errors;
                $this->data['request'] = $_POST;
            }
        }

        $this->data['post'] = $post;
        // Get post comments.
        $this->data['comments'] = $comments->getComments($id);
    }
}