<?php

namespace app\controllers;

use app\models\Blog;

/**
 * Default site controller.
 */
class DefaultController extends \Core\Controller
{
    /**
     * Action for index page.
     */
    public function actionIndex()
    {
        // Load model.
        $blog = new Blog();

        // Create post if form was submited.
        if (!empty($_POST)) {
            $data = $_POST;

            // Validation.
            if (empty($data['author'])) {
                $errors['author'] = 'This field is required!';
            }
            if (empty($data['content'])) {
                $errors['content'] = 'This field is required!';
            }

            if (empty($errors)) {
                if ($blog->createPost($data)) {
                    $result = [
                        'success' => true,
                        'message' => 'Post created successfully!',
                    ];
                } else {
                    $result = [
                        'success' => true,
                        'message' => 'Error on post creating!',
                    ];
                }
                $this->data['result'] = $result;
            } else {
                $this->data['errors'] = $errors;
                $this->data['request'] = $_POST;
            }
        }

        // Get latest posts.
        $this->data['posts'] = $blog->getPosts(5);
        // Get most commented posts.
        $this->data['mostCommentedPosts'] = $blog->getMostCommented();
    }
}