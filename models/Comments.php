<?php

namespace app\models;

/**
 * Comments model class.
 */
class Comments extends \Core\Model
{
    /**
     * Get all comments.
     *
     * @param int $postId
     * @return array
     */
    public function getComments($postId)
    {
        $sql = "SELECT *
                FROM comments
                WHERE post_id = :post_id
                ORDER BY created_at DESC";
        $query = $this->db->prepare($sql);
        $query->execute([':post_id' => $postId]);

        return $query->fetchAll();
    }

    /**
     * Create comment.
     *
     * @param array $data
     * @return bool
     */
    public function createComment($data)
    {
        $sql = "INSERT INTO comments (author, comment, post_id) VALUES (:author, :comment, :post_id)";
        $query = $this->db->prepare($sql);
        $parameters = [
            ':author' => strip_tags(htmlspecialchars($data['author'])),
            ':comment' => strip_tags(htmlspecialchars($data['comment'])),
            ':post_id' => $data['postId'],
        ];

        return $query->execute($parameters);
    }
}