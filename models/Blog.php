<?php

namespace app\models;

/**
 * Blog model class.
 */
class Blog extends \Core\Model
{
    /**
     * Get all posts.
     *
     * @param mixed $limit
     * @return array
     */
    public function getPosts($limit = null)
    {
        $sql = "SELECT blog.*, COUNT(comments.id) AS comments
                FROM blog
                LEFT JOIN comments ON comments.post_id = blog.id
                GROUP BY blog.id
                ORDER BY created_at DESC";

        if (is_int($limit)) {
            $sql .= " LIMIT {$limit}";
        }

        $query = $this->db->prepare($sql);
        $query->execute();

        return $query->fetchAll();
    }

    /**
     * Get one post.
     *
     * @param int $id
     * @return array
     */
    public function getPost($id)
    {
        $sql = "SELECT *
                FROM blog
                WHERE id = :id";
        $query = $this->db->prepare($sql);
        $query->execute([':id' => $id]);

        return $query->fetch();
    }

    /**
     * Get all posts.
     *
     * @param int $limit
     * @return array
     */
    public function getMostCommented($limit = 5)
    {
        if (is_int($limit)) {
            $limit = 5;
        }

        $sql = "SELECT blog.*, COUNT(comments.id) AS comments
                FROM blog
                LEFT JOIN comments ON comments.post_id = blog.id
                GROUP BY blog.id
                HAVING comments > 0
                ORDER BY comments DESC
                LIMIT {$limit}";
        $query = $this->db->prepare($sql);
        $query->execute();

        return $query->fetchAll();
    }

    /**
     * Create post.
     *
     * @param array $data
     * @return booll
     */
    public function createPost($data)
    {
        $sql = "INSERT INTO blog (author, content) VALUES (:author, :content)";
        $query = $this->db->prepare($sql);
        $parameters = [
            ':author' => strip_tags(htmlspecialchars($data['author'])),
            ':content' => strip_tags(htmlspecialchars($data['content'])),
        ];

        return $query->execute($parameters);
    }
}