<?php

namespace Core;

use \Exception;

/**
 * Application class.
 *
 * @property Router $router
 * @property PDO $db connection to db.
 */
class App
{
    protected static $router;
    protected static $db;

    /**
     * Get router.
     */
    public function getRouter()
    {
        return self::$router;
    }

    /**
     * Run application.
     *
     * @param string $uri
     */
    public static function run($uri)
    {
        // Set router.
        self::$router = new Router($uri);
        // Create db connection.
        self::$db = (new Db(
            Config::get('dbHost'),
            Config::get('dbUser'),
            Config::get('dbPassword'),
            Config::get('dbName'),
            Config::get('dbChar')
        ))->getConnection();

        $controllerClass = Config::get('controllersNamespace') . ucfirst(self::$router->getController()) . 'Controller';
        $controllerMethod = 'action' . ucfirst(strtolower(self::$router->getMethodPrefix())) . ucfirst(strtolower(self::$router->getAction()));

        // Calling controller's method.
        $controller = new $controllerClass();
        if (method_exists($controller, $controllerMethod)) {
            // Controller's action may return a view path.
            $viewPath = $controller->$controllerMethod();
            $view = new View($controller->getData(), $viewPath);
            $content = $view->render();
        } else {
            throw new Exception("Method {$controllerMethod} of class {$controllerClass} doesn`t exists.");
        }

        $layoutName = self::$router->getRoute();
        $layoutPath = VIEWS_PATH . $layoutName . '_layout.php';
        $layout = new View(['content' => $content], $layoutPath);

        echo $layout->render();
    }

    /**
     * Get db.
     */
    public static function getDb()
    {
        return self::$db;
    }
}