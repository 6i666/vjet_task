<?php

namespace Core;

/**
 * System config class.
 *
 * @property array $settings
 */
class Config
{
    protected static $settings = [];

    /**
     * Add settings.
     *
     * @param string $key setting name
     * @param mixed $value setting value
     */
    public static function set($key, $value)
    {
        self::$settings[$key] = $value;
    }

    /**
     * Add settings multiple.
     *
     * @param array $settings
     */
    public static function setMultiple($settings)
    {
        foreach ($settings as $key => $value) {
            self::set($key, $value);
        }
    }

    /**
     * Get setting.
     *
     * @return mixed
     */
    public static function get($key)
    {
        return isset(self::$settings[$key]) ? self::$settings[$key] : null;
    }
}