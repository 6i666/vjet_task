<?php
/**
 * @file
 *
 * Application initialization, set config.
 */


// Load config.
$settings = CORE_PATH . 'config' . DS . 'config.php';

if (file_exists($settings)) {
    Core\Config::setMultiple(require_once($settings));
} else {
    throw new \Exception('Config file doesn`t exists: ' . $settings);
}
