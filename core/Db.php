<?php

namespace Core;

use \PDO;

/**
 * Database connection class.
 *
 * @property PDO $connection
 */
class Db
{
    protected $connection;

    /**
     * Connect to db.
     *
     * @param string $host database host
     * @param string $user database user
     * @param string $password database password
     * @param string $dbName database name
     */
    public function __construct($host, $user, $pass, $dbName, $dbChar)
    {
        try {
            $opt = [
                PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION,
                PDO::ATTR_DEFAULT_FETCH_MODE => PDO::FETCH_ASSOC,
                PDO::ATTR_EMULATE_PREPARES => TRUE,
            ];
            $dsn = "mysql:host={$host};dbname={$dbName};charset={$dbChar}";

            $this->connection = new PDO($dsn, $user, $pass, $opt);
        } catch (PDOException $e) {
            echo "Error!: " . $e->getMessage();
            die;
        }
    }

    /**
     * Get connection.
     * @return PDO connection
     */
    public function getConnection()
    {
        return $this->connection;
    }
}