<?php

namespace Core;

/**
 * Main controller class.
 *
 * @propperty array $data data for view
 * @propperty object $model model object
 * @propperty array $params url params
 */
class Controller
{
    protected $data;
    protected $model;
    protected $params;

    /**
     * Constructor.
     *
     * @param array $data
     */
    public function __construct($data = [])
    {
        $this->data = $data;
        $this->params = App::getRouter()->getParams();
        $this->init();
    }

    /**
     * Set model.
     */
    protected function init()
    {
        // Set model here.
    }

    /**
     * Get router
     */
    public function getData()
    {
        return $this->data;
    }

    /**
     * Get router
     */
    public function getModel()
    {
        return $this->model;
    }

    /**
     * Get router
     */
    public function getParams()
    {
        return $this->params;
    }
}