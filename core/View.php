<?php

namespace Core;

use \Exception;

/**
 * Main view class.
 *
 * @propperty array $data data for view
 * @propperty string $path path to view file
 */
class View
{
    protected $data;
    protected $path;

    /**
     * Constructor.
     *
     * @param array $data
     * @param mixad $path
     */
    public function __construct($data = [], $path = null)
    {
        if (!$path) {
            $path = self::getDefaultViewPath();
        }
        if (!file_exists($path)) {
            throw new Exception('Template file isn`t found in: ' . $path);
        }

        $this->data = $data;
        $this->path = $path;
    }

    /**
     * Get default view path.
     *
     * @return mixed
     */
    public static function getDefaultViewPath()
    {
        $router = App::getRouter();

        if (!$router) {
            return false;
        }

        $controllerDir = $router->getController();
        $templateName = $router->getMethodPrefix() . $router->getAction() . '.php';

        return VIEWS_PATH . $controllerDir . DS . $templateName;
    }

    /**
     * Render view.
     *
     * @return mixed
     */
    public function render()
    {
        $data = $this->data;

        ob_start();
        include($this->path);
        $content = ob_get_clean();

        return $content;
    }
}