<?php

namespace Core;

/**
 * Router class.
 *
 * @property string $uri requested url
 * @property string $controller controller name
 * @property string $action action name
 * @property array $params params fron url
 * @property string $route site route
 * @property string $methodPrefix
 */
class Router
{
    protected $uri;
    protected $controller;
    protected $action;
    protected $params;
    protected $route;
    protected $methodPrefix;

    /**
     * Class constructor.
     *
     * @param string $uri
     */
    public function __construct($uri)
    {
        $this->uri = urldecode(trim($uri , '/'));

        // Get defaults.
        $routes = Config::get('routes');
        $this->route = Config::get('defaultRoute');
        $this->methodPrefix = isset($routes[$this->route]) ? $routes[$this->route] : '';
        $this->controller = Config::get('defaultController');
        $this->action = Config::get('defaultAction');

        // Parse url.
        $urlParts = parse_url($this->uri);

        // Get path parts.
        $pathParts = explode('/', strtolower($urlParts['path']));

        if (!empty($pathParts)) {
            // Get route at first element.
            if (in_array(current($pathParts), array_keys($routes))) {
                $this->route = current($pathParts);
                array_shift($pathParts);
            }

            // Get controller.
            if (current($pathParts)) {
                $this->controller = current($pathParts);
                array_shift($pathParts);
            }

            // Get action.
            if (current($pathParts)) {
                $this->action = current($pathParts);
                array_shift($pathParts);
            }

            // Get params.
            $this->params = $pathParts;
        }
        // var_dump($pathParts);
    }

    /**
     * Get uri.
     *
     * @return mixed
     */
    public function getUri()
    {
        return $this->uri;
    }

    /**
     * Get controller.
     *
     * @return mixed
     */
    public function getController()
    {
        return $this->controller;
    }

    /**
     * Get action.
     *
     * @return mixed
     */
    public function getAction()
    {
        return $this->action;
    }

    /**
     * Get params.
     *
     * @return mixed
     */
    public function getParams()
    {
        return $this->params;
    }

    /**
     * Get route.
     *
     * @return mixed
     */
    public function getRoute()
    {
        return $this->route;
    }

    /**
     * Get method prefix.
     *
     * @return mixed
     */
    public function getMethodPrefix()
    {
        return $this->methodPrefix;
    }
}