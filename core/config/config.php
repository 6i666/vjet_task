<?php
/**
 * @file
 * Site settings.
 */

return [
    'siteName' => 'Test Blog',
    'routes' => [
        'default' => '',
        // 'blog' => 'blog',
    ],
    'defaultRoute' => 'default',
    'defaultController' => 'default',
    'defaultAction' => 'index',
    'controllersNamespace' => 'app\controllers\\',
    // Db settings.
    'dbHost' => 'localhost',
    'dbName' => 'vjet_blog',
    'dbUser' => 'root',
    'dbPassword' => 'root',
    'dbChar' => 'utf8',
];
