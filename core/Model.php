<?php

namespace Core;

/**
 * Model class.
 *
 * @property PDO $db connection to db.
 */
class Model
{
    protected $db;

    /**
     * Set db connection.
     */
    public function __construct()
    {
        $this->db = App::getDb();
    }
}