<hr>

<h2 class="text-center">Blog Posts</h2>

<hr>

<?php foreach ($data['posts'] as $post): ?>
    <h3 class="media-heading"><?=$post['author']?>
        <small><?=$post['created_at']?></small>
        <small class="pull-right">Comments(<?=$post['comments']?>)</small>
    </h3>
    <!-- Post Content -->
    <?=substr($post['content'], 0, 100)?>
    <p class="lead">
        <a href="blog/post/<?=$post['id']?>">Read more</a>
    </p>
    <hr>
<?php endforeach; ?>