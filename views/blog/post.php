<h1><small>author</small> <?=$data['post']['author']?></h1>

<hr>

<!-- Date/Time -->
<p><span class="glyphicon glyphicon-time"></span> Posted on <?=$data['post']['created_at']?></p>

<hr>

<!-- Post Content -->
<p><?=$data['post']['content']?></p>

<hr>

<!-- Blog Comments -->

<!-- Comments Form -->
<?php if (isset($data['result'])): ?>
    <div class="alert alert-<?php echo $data['result']['success'] == true ? 'success' : 'danger'; ?>">
        <?=$data['result']['message']?>
    </div>
<?php endif; ?>

<div class="well">
    <h4>Leave a Comment:</h4>
    <form role="form" method="post">
        <div class="form-group">
            <input type="text" class="form-control" id="author" name="author" placeholder="Your name" value="<?php echo isset($data['request']['author']) ? $data['request']['author'] : ''; ?>">
            <?php if (isset($data['errors']['author'])): ?>
                <small class="form-text text-muted text-danger"><?=$data['errors']['author']?></small>
            <?php endif; ?>
        </div>
        <div class="form-group">
            <textarea class="form-control" id="text" rows="3" name="comment" placeholder="Content.."><?php echo isset($data['request']['comment']) ? $data['request']['comment'] : ''; ?></textarea>
            <?php if (isset($data['errors']['comment'])): ?>
                <small class="form-text text-muted text-danger"><?=$data['errors']['comment']?></small>
            <?php endif; ?>
        </div>
        <button type="submit" class="btn btn-primary" name="create_comment">Create</button>
    </form>
</div>

<hr>

<!-- Posted Comments -->
<?php foreach ($data['comments'] as $comment): ?>
    <!-- Comment -->
    <div class="media">
        <div class="media-body">
            <h4 class="media-heading">
                <small>author</small> <?=$comment['author']?>
                <small><?=$comment['created_at']?></small>
            </h4>
            <?=$comment['comment']?>
        </div>
    </div>
<?php endforeach; ?>