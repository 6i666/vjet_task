<h1>Mini Blog</h1>

<!-- Create post Form -->
<?php if (isset($data['result'])): ?>
    <div class="alert alert-<?php echo $data['result']['success'] == true ? 'success' : 'danger'; ?>">
        <?=$data['result']['message']?>
    </div>
<?php endif; ?>

<div class="well">
    <h4>Create post:</h4>
    <form role="form" method="post">
        <div class="form-group">
            <label for="author">Author</label>
            <input type="text" class="form-control" id="author" name="author" placeholder="Your name" value="<?php echo isset($data['request']['author']) ? $data['request']['author'] : ''; ?>">
            <?php if (isset($data['errors']['author'])): ?>
                <small class="form-text text-muted text-danger"><?=$data['errors']['author']?></small>
            <?php endif; ?>
        </div>
        <div class="form-group">
            <label for="text">Post content</label>
            <textarea class="form-control" id="text" rows="3" name="content" placeholder="Content.."><?php echo isset($data['request']['content']) ? $data['request']['content'] : ''; ?></textarea>
            <?php if (isset($data['errors']['content'])): ?>
                <small class="form-text text-muted text-danger"><?=$data['errors']['content']?></small>
            <?php endif; ?>
        </div>
        <button type="submit" class="btn btn-primary" name="create_post">Create</button>
    </form>
</div>

<hr>

<h2 class="text-center">Most discussed</h2>

<hr>

<div id="myCarousel" class="carousel slide" data-ride="carousel">
    <!-- Wrapper for slides -->
    <div class="carousel-inner" role="listbox">
        <?php $i = 0; foreach ($data['mostCommentedPosts'] as $post):  ?>
            <div class="item <?php echo $i > 0 ? '' : 'active'; ?>" style="padding: 20px 20px 0 20px;">
                <h3 class="media-heading"><?=$post['author']?>
                    <small><?=$post['created_at']?></small>
                    <small class="pull-right">Comments(<?=$post['comments']?>)</small>
                </h3>
                <!-- Post Content -->
                <?=substr($post['content'], 0, 100)?>
                <p class="lead text-center">
                    <a href="blog/post/<?=$post['id']?>">Read more</a>
                </p>
            </div>
        <?php $i++; endforeach; ?>
    </div>

    <!-- Left and right controls -->
    <a class="left carousel-control" href="#myCarousel" role="button" data-slide="prev">
        <span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
        <span class="sr-only">Previous</span>
    </a>
    <a class="right carousel-control" href="#myCarousel" role="button" data-slide="next">
        <span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
        <span class="sr-only">Next</span>
    </a>
</div>

<hr>

<h2 class="text-center">Latest Posts</h2>

<hr>

<?php foreach ($data['posts'] as $post): ?>
    <h3 class="media-heading"><?=$post['author']?>
        <small><?=$post['created_at']?></small>
        <small class="pull-right">Comments(<?=$post['comments']?>)</small>
    </h3>
    <!-- Post Content -->
    <?=substr($post['content'], 0, 100)?>
    <p class="lead">
        <a href="blog/post/<?=$post['id']?>">Read more</a>
    </p>
    <hr>
<?php endforeach; ?>

<div class="text-center"><a href="blog" class="btn btn-lg btn-primary">Read all</a></div>